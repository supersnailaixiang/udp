package main 
import (
	"net"
	"fmt"
	"time"
)
//var count int = 1  不靠谱，并发的时候++操作不是原子操作
func read(conn *net.UDPConn, c chan int){
	<-c // 不对这个应该检测另外一个是否读写，而不是这个
	data := make([]byte,1024)
	n, remoteAddr, err := conn.ReadFromUDP(data)
	 if err != nil {
	 	fmt.Printf("read error %s from %s\n",err,remoteAddr)
	 }
	 fmt.Printf("read: %s bytes:%n from %s\n",data,n,remoteAddr)
}
func handle(c1 chan int, c2 chan int ,srcAddr *net.UDPAddr,dstAddr *net.UDPAddr){

	listener,err := net.ListenUDP("udp",srcAddr) // 监听服务器。
	if err != nil {
		fmt.Println(err)
	}
	 
	/* data := make([]byte,1024)
	 n, remoteAddr, err := listener.ReadFromUDP(data) // 两个都在等待信息，死锁了，所以读写应该是异步的。

	 if err != nil {
	 	fmt.Printf("read error %s from %s",err,remoteAddr)
	 }
	
	 fmt.Printf("read: %s bytes:%n from %s",data,n,remoteAddr)
	 */
	 go read(listener,c2)
	 listener.WriteToUDP([]byte("I have Received from information\n "),dstAddr) 
 	 
		c1 <- 1
	 // 第一个协程走到这里走到这里结束了，下一个协程发送数据怎么办？
	 // 开启互斥锁？这个应该是互相等待问题，所以一个是搞不定的而且不能搞定，那么就不用
	 // 同步异步锁太重要了mutex sync.RWMutex ，两个都结束才能
	 // 这个地方也需要等待才行，要等待对方的信息返回信息才可以结束


}
func main(){
	addr1 := &net.UDPAddr{IP:net.ParseIP("127.0.0.1"),Port:8081}
	addr2 := &net.UDPAddr{IP:net.ParseIP("127.0.0.1"),Port:8082}
 
	//lock := &sync.RWMutex{}
 
	chs := make([]chan int,2)
 
	for i :=0; i<2; i++ {
		chs[i] = make(chan int)
	}
 

	go handle(chs[0],chs[1],addr1,addr2)
	go handle(chs[1],chs[0],addr2,addr1)
 	 
 	 time.Sleep(5*time.Second)

	
}