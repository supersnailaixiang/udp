package main 
import (
	"fmt"
//	"os"
	"net"
)
func main(){
	sip := net.ParseIP("127.0.0.1")
	srcAddr := &net.UDPAddr{IP:sip,Port:0}
	dstAddr := &net.UDPAddr{IP:sip,Port:9981}
	 

	conn, err := net.DialUDP("udp",srcAddr,dstAddr)
	if err != nil {
		fmt.Println(err)
	}
	defer conn.Close()
	//b := make([]byte,1)
	//os.Stdin.Read(b) // 没有读取所以一直打开
	conn.Write([]byte("hello"))
	fmt.Printf("<%s>\n",conn.RemoteAddr())

	data := make([]byte,1024)
	n,err := conn.Read(data)
	if err != nil {
		fmt.Println("read error:",err)
	}
	fmt.Printf("read %s bytes: %d from %s",data,n,conn.RemoteAddr())
 
}